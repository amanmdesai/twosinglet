# import all the functions in twosinglet_scalarcouplings.py:
from twosinglet_scalarcouplings import *

# import all the functions for the HiggsBounds/HiggsSignals constraints:
from twosinglet_check_higgs_sigbound import *
from prettytable import MSWORD_FRIENDLY, PLAIN_COLUMNS

# output directory for any plots
outputdirectory = 'plots/'

# which mode to run
TESTONEPOINT = False # test for a single point
SCANPOINTS = True

if TESTONEPOINT is True:
    v = 246.
    vs = 140.
    vx = 100.
    M1 = 125.09
    M2 = 300.
    M3 = 450.
    
    a12=-0.129
    a13= 0.226
    a23=-0.899

    #a12=0.01
    #a13=0.01
    #a23=-0.999
    
    # generate the point data
    v, vs, vx, M1, M2, M3, a12, a13, a23, R, lambda_dict, xs13_n3lo_h1, xs13_n3lo_h2, xs13_n3lo_h3, h1_BRs, h2_BRs, h3_BRs, paramsubs = get_point_info(v, vs, vx, M1, M2, M3, a12, a13, a23, True)

    # convert the point data into a format for HB/HS:
    twosinglet_point_info = [] # array should contain all the points 
    test_point = convert_to_HBHS("TESTPOINT", v, vs, vx, M1, M2, M3, a12, a13, a23, R, lambda_dict, xs13_n3lo_h1, xs13_n3lo_h2, xs13_n3lo_h3, h1_BRs, h2_BRs, h3_BRs, paramsubs)
    twosinglet_point_info.append(test_point)

    # launch HiggsBounds and HiggsSignals and get the results:
    HB_results, HS_results = get_HiggsBoundsSignals_results('.', 'TwoSinglet', twosinglet_point_info)

    # print the results:
    table = print_HBHS_results(HB_results, HS_results, twosinglet_point_info)

 

# scan over the BP3 points:
if SCANPOINTS is True:
    # parameters that remain fixed:
    v = 246.
    vs = 140.
    vx = 100.
    M1 = 125.09
    a12=-0.129
    a13= 0.226
    a23=-0.899

    # create arrays of M2 and M3:
    M2_min = 126.
    M2_max = 800.
    M3_min = 255.
    M3_max = 850.
    NPOINTS = 25 # the number of points in each direction
    
    M2_array = np.linspace(M2_min, M2_max, NPOINTS)
    M3_array = np.linspace(M3_min, M3_max, NPOINTS)

    twosinglet_point_info = [] # array should contain all the points


    
    for M2 in M2_array:
        for M3 in M3_array:
            if M2 < M3:
                name = "M2_" + str(round_sig(M2,3)) + "_M3_" + str(round_sig(M3,3))
                # generate the point data
                v, vs, vx, M1, M2, M3, a12, a13, a23, R, lambda_dict, xs13_n3lo_h1, xs13_n3lo_h2, xs13_n3lo_h3, h1_BRs, h2_BRs, h3_BRs, paramsubs = get_point_info(v, vs, vx, M1, M2, M3, a12, a13, a23, True)
                current_point = convert_to_HBHS(name, v, vs, vx, M1, M2, M3, a12, a13, a23, R, lambda_dict, xs13_n3lo_h1, xs13_n3lo_h2, xs13_n3lo_h3, h1_BRs, h2_BRs, h3_BRs, paramsubs)
                twosinglet_point_info.append(current_point)
                
    # launch HiggsBounds and HiggsSignals and get the results:
    HB_results, HS_results = get_HiggsBoundsSignals_results('.', 'TwoSinglet', twosinglet_point_info)

    # print the results:
    table = print_HBHS_results(HB_results, HS_results, twosinglet_point_info)

    # write out the table
    table.set_style(PLAIN_COLUMNS)
    table_txt = table.get_string()
    table_output = open('table_output.txt','w')
    table_output.write(table_txt)
    table_output.close()

    # arrays for the plots:
    Constraints_HBHS_pass = []
    Constraints_HLLHC_pass = []
    M2_scatter = []
    M3_scatter = []

    # loop over constraint results and fill arrays for the plots
    for p in range(len(twosinglet_point_info)):

        M2 = twosinglet_point_info[p][2]
        M3 = twosinglet_point_info[p][3]

        # get the cross sections and branching ratios
        xs13_n3lo_h2 = twosinglet_point_info[p][13]
        xs13_n3lo_h2 = twosinglet_point_info[p][14]

        BR_WW_h2 = twosinglet_point_info[p][11][9]
        BR_ZZ_h2 = twosinglet_point_info[p][11][10]
        BR_h1h1_h2 = twosinglet_point_info[p][11][11]

        BR_WW_h3 = twosinglet_point_info[p][12][9]
        BR_ZZ_h3 = twosinglet_point_info[p][12][10]
        BR_h1h1_h3 = twosinglet_point_info[p][12][11]
        
        xs13_ZZ_h2 = xs13_n3lo_h2 * BR_ZZ_h2
        xs13_WW_h2 = xs13_n3lo_h2 * BR_WW_h2
        xs13_HH_h2 = xs13_n3lo_h2 * BR_h1h1_h2

        xs13_ZZ_h3 = xs13_n3lo_h3 * BR_ZZ_h3
        xs13_WW_h3 = xs13_n3lo_h3 * BR_WW_h3
        xs13_HH_h3 = xs13_n3lo_h3 * BR_h1h1_h3

        # get the HL-LHC projections (h2):
        limit_HLLHC_ZZ_central_h2, limit_HLLHC_ZZ_1sigma_h2, limit_HLLHC_ZZ_2sigma_h2, HLLHC_ZZ_process_tag_h2 = get_xsec_limit_future(M2, HLLHC_ZZ_central, HLLHC_ZZ_1sigma, HLLHC_ZZ_2sigma, HLLHC_ZZ_process_tag)
        limit_HLLHC_WW_central_h2, limit_HLLHC_WW_1sigma_h2, limit_HLLHC_WW_2sigma_h2, HLLHC_WW_process_tag_h2 = get_xsec_limit_future(M2, HLLHC_WW_central, HLLHC_WW_1sigma, HLLHC_WW_2sigma, HLLHC_WW_process_tag)
        
        # current ATLAS/CMS HH limits (h2):
        limit_ATLAS_HH_central_h2, ATLAS_HH_process_tag_h2 = get_xsec_limit_current(M2, ATLAS_HH_central, ATLAS_HH_process_tag) # 27.5-36.1/fb
        limit_CMS_HH_central_h2, CMS_HH_process_tag_h2 = get_xsec_limit_current(M2, CMS_HH_central, CMS_HH_process_tag) # 35.9/fb
        
        # current ATLAS WW/CMS ZZ limits (h2):
        limit_ATLAS_WW_central_h2, ATLAS_WW_process_tag_h2 = get_xsec_limit_current(M2, ATLAS_WW_central, ATLAS_WW_process_tag) # 36.1/fb
        limit_CMS_ZZ_central_h2, CMS_ZZ_process_tag_h2 = get_xsec_limit_current(M2, CMS_ZZ_central, CMS_ZZ_process_tag) # 35.9/fb
        
        # extrapolate naively to 3000/fb (h2):
        limit_ATLAS_HH_extrap_h2 = limit_ATLAS_HH_central_h2 * math.sqrt(36.1/3000.)
        limit_CMS_HH_extrap_h2 = limit_CMS_HH_central_h2 * math.sqrt(35.9/3000.)
        limit_ATLAS_WW_extrap_h2 = limit_ATLAS_WW_central_h2 * math.sqrt(36.1/3000.)
        limit_CMS_ZZ_extrap_h2 = limit_CMS_ZZ_central_h2 * math.sqrt(35.9/3000.)

        # get the HL-LHC projections (h3):
        limit_HLLHC_ZZ_central_h3, limit_HLLHC_ZZ_1sigma_h3, limit_HLLHC_ZZ_2sigma_h3, HLLHC_ZZ_process_tag_h3 = get_xsec_limit_future(M3, HLLHC_ZZ_central, HLLHC_ZZ_1sigma, HLLHC_ZZ_2sigma, HLLHC_ZZ_process_tag)
        limit_HLLHC_WW_central_h3, limit_HLLHC_WW_1sigma_h3, limit_HLLHC_WW_2sigma_h3, HLLHC_WW_process_tag_h3 = get_xsec_limit_future(M3, HLLHC_WW_central, HLLHC_WW_1sigma, HLLHC_WW_2sigma, HLLHC_WW_process_tag)
        
        # current ATLAS/CMS HH limits (h3):
        limit_ATLAS_HH_central_h3, ATLAS_HH_process_tag_h3 = get_xsec_limit_current(M3, ATLAS_HH_central, ATLAS_HH_process_tag) # 27.5-36.1/fb
        limit_CMS_HH_central_h3, CMS_HH_process_tag_h3 = get_xsec_limit_current(M3, CMS_HH_central, CMS_HH_process_tag) # 35.9/fb
        
        # current ATLAS WW/CMS ZZ limits (h3):
        limit_ATLAS_WW_central_h3, ATLAS_WW_process_tag_h3 = get_xsec_limit_current(M3, ATLAS_WW_central, ATLAS_WW_process_tag) # 36.1/fb
        limit_CMS_ZZ_central_h3, CMS_ZZ_process_tag_h3 = get_xsec_limit_current(M3, CMS_ZZ_central, CMS_ZZ_process_tag) # 35.9/fb
        
        # extrapolate naively to 3000/fb (h3):
        limit_ATLAS_HH_extrap_h3 = limit_ATLAS_HH_central_h3 * math.sqrt(36.1/3000.)
        limit_CMS_HH_extrap_h3 = limit_CMS_HH_central_h3 * math.sqrt(35.9/3000.)
        limit_ATLAS_WW_extrap_h3 = limit_ATLAS_WW_central_h3 * math.sqrt(36.1/3000.)
        limit_CMS_ZZ_extrap_h3 = limit_CMS_ZZ_central_h3 * math.sqrt(35.9/3000.)


        # ATLAS CURRENT HH (h2)
        if xs13_HH_h2 > limit_ATLAS_HH_central_h2:
            ATLAS_HH_h2 = 0
        else:
            ATLAS_HH_h2 = 1
        # CMS CURRENT HH:
        if xs13_HH_h2 > limit_CMS_HH_central_h2:
            CMS_HH_h2 = 0
        else:
            CMS_HH_h2 = 1
        # ATLAS CURRENT HH (h3)
        if xs13_HH_h3 > limit_ATLAS_HH_central_h3:
            ATLAS_HH_h3 = 0
        else:
            ATLAS_HH_h3 = 1
        # CMS CURRENT HH:
        if xs13_HH_h3 > limit_CMS_HH_central_h3:
            CMS_HH_h3 = 0
        else:
            CMS_HH_h3 = 1

        # CHECK HL-LHC (h2)
        # HL-LHC ZZ:
        if xs13_ZZ_h2 > limit_HLLHC_ZZ_central_h2:
            HLLHC_ZZ_h2 = 0
        else:
            HLLHC_ZZ_h2 = 1
        # HL-LHC WW:
        if xs13_WW_h2 > limit_HLLHC_WW_central_h2:
            HLLHC_WW_h2 = 0
        else:
            HLLHC_WW_h2 = 1
        # ATLAS HH HL-LHC extrap.:
        if xs13_HH_h2 > limit_ATLAS_HH_extrap_h2:
            ATLAS_HH_extrap_h2 = 0
        else:
            ATLAS_HH_extrap_h2 = 1
        # CMS HH HL-LHC extrap.:
        if xs13_HH_h2 > limit_CMS_HH_extrap_h2:
            CMS_HH_extrap_h2 = 0
        else:
            CMS_HH_extrap_h2 = 1
        # ATLAS WW HL-LHC extrap., if mh2 < 550 GeV:
        if M2 < 550.:
            if xs13_WW_h2 > limit_ATLAS_WW_extrap_h2:
                ATLAS_WW_extrap_h2 = 0
            else:
                ATLAS_WW_extrap_h2 = 1
            # CMS ZZ HL-LHC extrap.:
            if xs13_ZZ_h2 > limit_CMS_ZZ_extrap_h2:
                CMS_ZZ_extrap_h2 = 0
            else:
                CMS_ZZ_extrap_h2 = 1
            HLLHC_ZZ_h2 = CMS_ZZ_extrap_h2
            HLLHC_WW_h2 = ATLAS_WW_extrap_h2

        # CHECK HL-LHC (h3)
        # HL-LHC ZZ:
        if xs13_ZZ_h3 > limit_HLLHC_ZZ_central_h3:
            HLLHC_ZZ_h3 = 0
        else:
            HLLHC_ZZ_h3 = 1
        # HL-LHC WW:
        if xs13_WW_h3 > limit_HLLHC_WW_central_h3:
            HLLHC_WW_h3 = 0
        else:
            HLLHC_WW_h3 = 1
        # ATLAS HH HL-LHC extrap.:
        if xs13_HH_h3 > limit_ATLAS_HH_extrap_h3:
            ATLAS_HH_extrap_h3 = 0
        else:
            ATLAS_HH_extrap_h3 = 1
        # CMS HH HL-LHC extrap.:
        if xs13_HH_h3 > limit_CMS_HH_extrap_h3:
            CMS_HH_extrap_h3 = 0
        else:
            CMS_HH_extrap_h3 = 1
        # ATLAS WW HL-LHC extrap., if mh3 < 550 GeV:
        if M2 < 550.:
            if xs13_WW_h3 > limit_ATLAS_WW_extrap_h3:
                ATLAS_WW_extrap_h3 = 0
            else:
                ATLAS_WW_extrap_h3 = 1
            # CMS ZZ HL-LHC extrap.:
            if xs13_ZZ_h3 > limit_CMS_ZZ_extrap_h3:
                CMS_ZZ_extrap_h3 = 0
            else:
                CMS_ZZ_extrap_h3 = 1
            HLLHC_ZZ_h3 = CMS_ZZ_extrap_h3
            HLLHC_WW_h3 = ATLAS_WW_extrap_h3

        # check if both h2 and h3 pass the HL-LHC constraints:
        if float(HLLHC_ZZ_h2) ==1 and float(HLLHC_WW_h2)==1 and float(ATLAS_HH_extrap_h2)==1 and float(CMS_HH_extrap_h2)==1 and float(HLLHC_ZZ_h3) ==1 and float(HLLHC_WW_h3)==1 and float(ATLAS_HH_extrap_h3)==1 and float(CMS_HH_extrap_h3)==1:
           HLLHC_pass = 'o'
        else:
           HLLHC_pass = 'x'
        
        # get the HiggsBounds/HiggsSignals results
        HB = str(HB_results[p])
        HS = str(HS_results[p])

        if int(HB) == 1 and float(HS) > 0.68 and float(ATLAS_HH_h2) == 1 and float(CMS_HH_h2) == 1 and float(ATLAS_HH_h3) == 1 and float(CMS_HH_h3):
            HBHS_pass = 'o'
        else:
            HBHS_pass = 'x'
        M2_scatter.append(M2)
        M3_scatter.append(M3)
        Constraints_HBHS_pass.append(HBHS_pass)
        Constraints_HLLHC_pass.append(HLLHC_pass)

    ##################################################
    # PRINT POINTS THAT PASSED HB+HS+hh constraints: #
    ##################################################

    print('passed HB+HS+hh:')
    HBHShhpass = open('passed_HBHShh.txt', 'w')
    for m in range(len(M2_scatter)):
        if Constraints_HBHS_pass[m] == 'o':
            print((M2_scatter[m], M3_scatter[m]))
            HBHShhpass.write(str(M2_scatter[m]) + '\t' + str(M3_scatter[m]) + '\n')
    HBHShhpass.close()


    ###################################################################################
    # scatter plot for BR_hh vs M2 from different groups of benchmarks    #
    ###################################################################################
    print('---')
    print('plotting M2-M3 constraints (HiggsBounds)')
    # plot settings ########
    plot_type = 'M2M3_constraints'
    # plot:
    # plot settings
    ylab = '$M_3$ [GeV]'
    xlab = '$M_2$ [GeV]'
    ymin = M3_min-100.
    ymax = M3_max+200.
    xmin = M2_min-100.
    xmax = M2_max+200.
    ylog = False
    xlog = False
    
    # construct the axes for the plot
    gs = gridspec.GridSpec(4, 4)
    fig = pl.figure()
    ax = fig.add_subplot(111)
    ax.grid(False)
    
    #for key in BR_hh_scatter.keys():
        #print OneLoop_sintheta[key], Tree_l112[key]
        #print BR_hh_scatter[key],  M2_scatter[key]
    #    scatter = mscatter(M2_scatter[key], BR_hh_scatter[key], m=np.array(Constraints[key]), c=next_color(), label='', s=4)
    #    
    scatter = mscatter(M2_scatter, M3_scatter, m=np.array(Constraints_HBHS_pass), c=next_color(), label='', s=25) 
    #pl.plot(np.nan, np.nan, marker='o', lw=0, color=same_color(), label='BP3')

    scatter = mscatter(M2_scatter, M3_scatter, m=np.array(Constraints_HLLHC_pass), c='red', label='', s=15) 


    pl.plot(np.nan, np.nan, marker='o', color='green', lw=0, label='Current')
    pl.plot(np.nan, np.nan, marker='o', color='red', lw=0, label='HL-LHC')
    pl.plot(np.nan, np.nan, marker='o', color='black', lw=0, label='Allowed')
    pl.plot(np.nan, np.nan, marker='x', color='black', lw=0, label='Excluded')


    # set the ticks, labels and limits 
    #ax.yaxis.set_major_locator(MultipleLocator(0.1))
    #ax.yaxis.set_minor_locator(MultipleLocator(0.05))
    #ax.xaxis.set_major_locator(MultipleLocator(0.2))
    #ax.xaxis.set_minor_locator(MultipleLocator(0.05))
    ax.set_ylabel(ylab, fontsize=20)
    ax.set_xlabel(xlab, fontsize=20)
    #pl.ylim([ymin,ymax])
    pl.xlim([xmin, xmax])
    # choose x and y log scales
    if ylog:
        ax.set_yscale('log')
    else:
        ax.set_yscale('linear')
    if xlog:
        ax.set_xscale('log')
    else:
        ax.set_xscale('linear')
        
    title_text = r'BP3' 
    plt.text(800, 500, title_text, fontsize=25)

    # create legend and plot/font size
    ax.legend()
    ax.legend(loc="upper right", numpoints=1, frameon=False, prop={'size':8})
    #pl.rcParams.update({'font.size': 15})
    #pl.rcParams['figure.figsize'] = 12, 12
    
    # save the figure
    print('saving the figure')
    # save the figure in PDF format
    infile = plot_type + '.dat'
    print('---')
    print(('output in', outputdirectory + infile.replace('.dat','.pdf')))
    pl.savefig(outputdirectory + infile.replace('.dat','.pdf'), bbox_inches='tight')
    pl.savefig(outputdirectory + infile.replace('.dat','.png'), bbox_inches='tight', scale=0.1)
    pl.close(fig)

